from django.contrib import admin
from .models import Store
# Register your models here.


class StoreAdmin(admin.ModelAdmin):
    list_display = ('title', 'store_manager', 'since_at', 'count_match', 'count_view',)


admin.site.register(Store,StoreAdmin)
