
# Create your models here.

# Python
from datetime import datetime

# Django

from django.db import models

# Django Rest Framework

# Local
from chium.users import  models as user_model



class Store(models.Model):
    store_manager = models.ForeignKey(user_model.User, on_delete=models.CASCADE, verbose_name='업체 관리자')
    title = models.CharField(max_lenth=64, blank=True, verbose_name='상호명')
    address1 = models.TextField(blank=True, verbose_name='주소')
    address2 = models.TextField(blank=True, verbose_name='상세 주소')
    since_at = models.DateField(verbose_name='경력 시작년도')
    area_count_review = models.IntegerField(help_text='리뷰 갯수', verbose_name='리뷰 갯수')
    count_match = models.IntegerField(default = 0, help_text='매칭 횟수', verbose_name='매칭 갯수')
    count_view = models.IntegerField(default = 0, help_text='조회수', verbose_name='조회수')

    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(default=datetime.now)

    def get_career_period(self):
        now = datetime.datetiem.utcnow().replace(tzinfo=utc)
        period = now - self.since_at
        return period


class StoreCity(Store):
    city = models.CharField(blank=True)


class Review(Store):
    user = models.ManyToManyField(user_model.User, verbose_name='리뷰한 사람')
    contents = models.TextField(blank=True, null=True, verbose_name='리뷰 내용')
    commercial_space_rate = models.FloatField(blank=True, null=True, verbose_name='상업 공간 점수')
    office_space_rate = models.FloatField(blank=True, null=True, verbose_name='사무 공간 점수')
    residential_space_rate = models.FloatField(blank=True, null=True, verbose_name='주거 공간 점수')
    facilities_waste_rate = models.FloatField(blank=True, null=True, verbose_name='시설물/폐기물 점수')

    def get_rate(self):
        rate = (self.commercial_space_rate + self.office_space_rate +
                self.residential_space_rate + self.facilities_waste_rate)/4.0
        return rate
