# Python
from datetime import datetime

# Django
from django.db import models


# Main Section
class PopUpManager(models.Manager):
    pass


class PopUpAvailableManager(PopUpManager):
    def get_queryset(self):
        super().get_queryset().filter(is_active=True, untill_at__gte=datetime.now())


class PopUp(models.Model):
    title = models.TextField(blank = True, verbose_name = '팝업 제목')
    content = models.TextField(blank=True, verbose_name='팝업 내용')
    is_active = models.BooleanField(default=False)
    until_at = models.DateTimeField()

    objects = PopUpManager()
    available = PopUpAvailableManager()
