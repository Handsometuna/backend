from django.contrib import admin
from .models import PopUp
# Register your models here.

class PopupAdmin(admin.ModelAdmin):
    list_display = ('title', 'content','is_active', 'until_at')

admin.site.register(PopUp,PopupAdmin)
