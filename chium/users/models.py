# Python
from datetime import datetime

# Django
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

#Local


class UserManager(BaseUserManager):
    def create_user(
        self, username, password, id_kakao = None, id_google = None, id_instagram = None,
        name = None, address1 = None, address2 = None, phone = None,
    ):
        user = self.model(username = username, password = password, id_kakao = id_kakao, id_google = id_google, id_facebook = id_facebook,
                          id_instagram = id_instagram, name = name, address1 = address1, address2 = address2,
                          phone = phone, image = image, is_admin = False)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password,):
        user = self.model(username = username, password = None, is_admin = True)
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractUser):
    name = models.CharField(blank=True, null = False, max_length= 32,verbose_name='사용자 이름')
    email = models.EmailField(blank = True , null = False, unique = True, verbose_name = '사용자 이메일')
    phone = models.TextField(blank = True, null = False, help_text= '휴대폰 번호', verbose_name = '휴대폰 번호')
    address1 = models.TextField(blank = True , help_text='주소', null = False, verbose_name= '주소')
    address2 = models.TextField(blank = True , help_text='상세 주소', verbose_name= '상세 주소')
    count_review = models.IntegerField(blank = True , default = 0, help_text = '리뷰 횟수',verbose_name= '리뷰 갯수')
    count_counsel = models.IntegerField(blank = True, default = 0, help_text = '상담 횟수', verbose_name= '상담 갯수')

    created = models.DateTimeField(default = datetime.now)
    modified = models.DateTimeField(default = datetime.now)

    def __str__(self):
        return '%s %s' % (self.id, self.username,)
