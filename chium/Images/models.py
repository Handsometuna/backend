
#Django
from django.db import models

#Python
from datetime import datetime


# Create your models here.

class Image:
    post_user = models.ImageField()
    post_store = models.ImageField()
    popup = models.ImageField()
    image = models.ImageField(blank=True, null=True)

    created = models.DateTimeField(default = datetime.now)
    modified = models.DateTimeField(default = datetime.now)
